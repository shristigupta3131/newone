
# Project Title
Jenkins Master Slave Architecture
Jenkins master slave architecture is used to manage distributed builds. Master slave nodes communicate through the TCP/IP. Main Jenkins server acts as the master node that manages slaves. Salve is a java executables that runs on a remote machine.
A brief description of what this project does and who it's for



![App Screenshot](https://gitlab.com/-/ide/project/shristi5/jenkins-master-slave/tree/main/-/Diagram/Diagram_-Jenkins_Master_Slave_Architecture.png/)

## Step by Step Procedure to Create Jenkins master slave Architecture

1. Create two instance jenkins master and jenkins slave on AWS.

2. Using Putty connected the jenkins master server.

3. Installed Jenkins on the master server and started & enabled jenkins on the same server.

4. Using the public ipv4 ip address and port 8080 .Opened Jenkins on the chrome and completed jenkins admin login procedure.

5. Jenkins master node is now ready.

6. In order to create a slave node one has to open the slave server and complete the java installation process.

7. After successful installation of java on the slave server, will create a jenkin slave node on the jenkins server.

8. Need to complete the credential part along with adding pem keysrequired for succesful slave node creation.

9. Now the Slave node is up and running succesfully.

10. Create a job bydefault it will run by the master node.

11. Open console output and copy the directory name.Now open your master server login into the paste the directory using (cd directory path).Using ls -ltr Check the successful build.

12. Now create another job(managed by master node) specify the restriction part as Jenkins-slave-1(Jenkins slave node name through which you want to run the job.

13. Open console of this another job output and copy the directory name.Now open your slave server ,the paste the directory using (cd directory path).Using ls -ltr Check the successful build here.